package ru.sberbank.argus.riskmetric.batchriskservicestarter.config;

import com.zaxxer.hikari.pool.HikariProxyResultSet;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Margaryan A.A.
 */
@ExtendWith(MockitoExtension.class)
class SpringBatchConfigTest {
    @InjectMocks
    SpringBatchConfig springBatchConfig;
    @Mock
    private TaskProperties taskProperties;
    @Mock
    private ERSMapperProperties ersMapperProperties;
    @Mock
    private JobBuilderFactory jobBuilderFactory;
    @Mock
    private DataSource dataSource;
    @Mock
    private StepBuilderFactory stepBuilderFactory;

    @Test
    @SneakyThrows
    public void mapRowTestSuccess() {
        //given
        ResultSet resultSet = mock(ResultSet.class);
        ResultSetMetaData resultSetMetaData = mock(ResultSetMetaData.class);
        when(resultSet.getMetaData()).thenReturn(resultSetMetaData);
        //when
        Map<String, String> stringStringMap = new SpringBatchConfig.MetricRowMapper().mapRow(resultSet, 1);
        //then
        Map<String, String> expectedResult = new HashMap<>();
        assertEquals(expectedResult, stringStringMap);
    }
}