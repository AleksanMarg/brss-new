INSERT INTO RISKMETRICRECORD
VALUES (1, '1-X', '1', '1-X', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
       (2, '1-Z', '1', '1-Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
       (3, '1-Y', '1', '1-Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
       (4, '1-W', '1', '1-W', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO FINANCIAL_STATEMENT
VALUES (1, '1-X', TO_DATE('2019/07/22', 'yyyy/mm/dd'), NULL, NULL, NULL),
       (2, '1-Z', TO_DATE('2019/07/22', 'yyyy/mm/dd'), NULL, NULL, NULL),
       (3, '1-Y', TO_DATE('2019/07/22', 'yyyy/mm/dd'), NULL, NULL, NULL),
       (4, '1-W', TO_DATE('2019/07/22', 'yyyy/mm/dd'), NULL, NULL, NULL);

