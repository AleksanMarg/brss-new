
package ru.sberbank.argus.riskmetric.batchriskservicestarter.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "riskService",
    "riskObject",
    "requestor",
    "businessProcess"
})
public class ExecuteRiskServiceRq {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    private RiskService riskService;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskObject")
    private List<RiskObject> riskObject = new ArrayList<RiskObject>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestor")
    private String requestor;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    private BusinessProcess businessProcess;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    public RiskService getRiskService() {
        return riskService;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    public void setRiskService(RiskService riskService) {
        this.riskService = riskService;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskObject")
    public List<RiskObject> getRiskObject() {
        return riskObject;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskObject")
    public void setRiskObject(List<RiskObject> riskObject) {
        this.riskObject = riskObject;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestor")
    public String getRequestor() {
        return requestor;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("requestor")
    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    public BusinessProcess getBusinessProcess() {
        return businessProcess;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    public void setBusinessProcess(BusinessProcess businessProcess) {
        this.businessProcess = businessProcess;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ExecuteRiskServiceRq.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("riskService");
        sb.append('=');
        sb.append(((this.riskService == null)?"<null>":this.riskService));
        sb.append(',');
        sb.append("riskObject");
        sb.append('=');
        sb.append(((this.riskObject == null)?"<null>":this.riskObject));
        sb.append(',');
        sb.append("requestor");
        sb.append('=');
        sb.append(((this.requestor == null)?"<null>":this.requestor));
        sb.append(',');
        sb.append("businessProcess");
        sb.append('=');
        sb.append(((this.businessProcess == null)?"<null>":this.businessProcess));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.riskService == null)? 0 :this.riskService.hashCode()));
        result = ((result* 31)+((this.businessProcess == null)? 0 :this.businessProcess.hashCode()));
        result = ((result* 31)+((this.riskObject == null)? 0 :this.riskObject.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.requestor == null)? 0 :this.requestor.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ExecuteRiskServiceRq) == false) {
            return false;
        }
        ExecuteRiskServiceRq rhs = ((ExecuteRiskServiceRq) other);
        return ((((((this.riskService == rhs.riskService)||((this.riskService!= null)&&this.riskService.equals(rhs.riskService)))&&((this.businessProcess == rhs.businessProcess)||((this.businessProcess!= null)&&this.businessProcess.equals(rhs.businessProcess))))&&((this.riskObject == rhs.riskObject)||((this.riskObject!= null)&&this.riskObject.equals(rhs.riskObject))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.requestor == rhs.requestor)||((this.requestor!= null)&&this.requestor.equals(rhs.requestor))));
    }

}
