package ru.sberbank.argus.riskmetric.batchriskservicestarter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Margaryan A.A.
 */
@Data
@Configuration
@EnableConfigurationProperties
@PropertySource(value ={"classpath:init_task.yml"}, factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties("task1")
public class TaskProperties {
    private String sql;
    private String url;
    private String cronExpression;
    private int step1ChunkSize;
}

