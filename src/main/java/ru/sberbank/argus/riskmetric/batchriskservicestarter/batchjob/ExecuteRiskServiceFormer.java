package ru.sberbank.argus.riskmetric.batchriskservicestarter.batchjob;

import lombok.RequiredArgsConstructor;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.config.ERSMapperProperties;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.model.ExecuteRiskServiceRq;

import java.util.Map;
import java.util.Objects;

/**
 * @author Margaryan A.A.
 */
@RequiredArgsConstructor

public class ExecuteRiskServiceFormer {
    private final ERSMapperProperties ersMapperProperties;

    public void formRequest(ExecuteRiskServiceRq request, Map<String, String> sourceEntity){
        for (ERSMapperProperties.ERSMapperProperty ersMapperProperty : ersMapperProperties.getPropertyList()) {
            String value = null;
            SpelParserConfiguration config = new SpelParserConfiguration(true, true);
            StandardEvaluationContext context = new StandardEvaluationContext(request);
            ExpressionParser expressionParser = new SpelExpressionParser(config);
            if(ersMapperProperty.getConstantValue()!=null){
                value = ersMapperProperty.getConstantValue();
            }
            else {
                for (Map.Entry<String, String> entry : sourceEntity.entrySet()) {
                    if (Objects.equals(ersMapperProperty.getSourceProperty(), entry.getKey().toUpperCase())) {
                        value = entry.getValue();
                    }
                }
            }
            expressionParser.parseExpression(ersMapperProperty.getTargetProperty()).setValue(context, value);
        }
    }
}
