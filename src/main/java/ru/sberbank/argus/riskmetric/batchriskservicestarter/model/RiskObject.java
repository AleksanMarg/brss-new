
package ru.sberbank.argus.riskmetric.batchriskservicestarter.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type"
})
public class RiskObject {

    /**
     * Id объекта риска
     * (Required)
     * 
     */
    @JsonProperty("id")
    @JsonPropertyDescription("Id \u043e\u0431\u044a\u0435\u043a\u0442\u0430 \u0440\u0438\u0441\u043a\u0430")
    private String id;
    /**
     * Тип объекта риска
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("\u0422\u0438\u043f \u043e\u0431\u044a\u0435\u043a\u0442\u0430 \u0440\u0438\u0441\u043a\u0430")
    private String type;

    /**
     * Id объекта риска
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * Id объекта риска
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Тип объекта риска
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * Тип объекта риска
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(RiskObject.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null)?"<null>":this.type));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.type == null)? 0 :this.type.hashCode()));
        result = ((result* 31)+((this.id == null)? 0 :this.id.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RiskObject) == false) {
            return false;
        }
        RiskObject rhs = ((RiskObject) other);
        return (((this.type == rhs.type)||((this.type!= null)&&this.type.equals(rhs.type)))&&((this.id == rhs.id)||((this.id!= null)&&this.id.equals(rhs.id))));
    }

}
