package ru.sberbank.argus.riskmetric.batchriskservicestarter.controller;

import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.config.TaskProperties;

@RestController
@RequiredArgsConstructor
public class MetricController {
    private final Job job;
    private final JobLauncher jobLauncher;
    private final TaskProperties taskProperties;

    @GetMapping("/start-job")
    @Scheduled(cron = "#{@taskProperties.cronExpression}")
    @SchedulerLock(name = "TaskScheduler_scheduledTask",
            lockAtLeastForString = "PT5M", lockAtMostForString = "PT14M")
    public void runBatchJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        System.out.println("Job is started");
        jobLauncher.run(job, new JobParametersBuilder()
                .addLong("time",System.currentTimeMillis()).toJobParameters());
    }
}
