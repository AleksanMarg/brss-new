package ru.sberbank.argus.riskmetric.batchriskservicestarter.batchjob;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemProcessor;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.config.ERSMapperProperties;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.model.ExecuteRiskServiceRq;

import java.util.Map;

/**
 * @author Margaryan A.A.
 */
@RequiredArgsConstructor
public class RiskServiceItemProcessor implements ItemProcessor<Map<String, String>, ExecuteRiskServiceRq> {
    private final ERSMapperProperties ersMapperProperties;

    @Override
    public ExecuteRiskServiceRq process(Map<String, String> sourceProperty) {
        ExecuteRiskServiceRq request = new ExecuteRiskServiceRq();
        ExecuteRiskServiceFormer executeRiskServiceFormer = new ExecuteRiskServiceFormer(ersMapperProperties);
        executeRiskServiceFormer.formRequest(request, sourceProperty);
        return request;
    }
}