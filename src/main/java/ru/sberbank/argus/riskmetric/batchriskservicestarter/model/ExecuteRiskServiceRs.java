
package ru.sberbank.argus.riskmetric.batchriskservicestarter.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "riskService",
    "task",
    "businessProcess",
    "message"
})
public class ExecuteRiskServiceRs {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    private RiskService riskService;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("task")
    private Task task;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    private BusinessProcess businessProcess;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    private List<Object> message = new ArrayList<Object>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    public RiskService getRiskService() {
        return riskService;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("riskService")
    public void setRiskService(RiskService riskService) {
        this.riskService = riskService;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("task")
    public Task getTask() {
        return task;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("task")
    public void setTask(Task task) {
        this.task = task;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    public BusinessProcess getBusinessProcess() {
        return businessProcess;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("businessProcess")
    public void setBusinessProcess(BusinessProcess businessProcess) {
        this.businessProcess = businessProcess;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public List<Object> getMessage() {
        return message;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("message")
    public void setMessage(List<Object> message) {
        this.message = message;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ExecuteRiskServiceRs.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("riskService");
        sb.append('=');
        sb.append(((this.riskService == null)?"<null>":this.riskService));
        sb.append(',');
        sb.append("task");
        sb.append('=');
        sb.append(((this.task == null)?"<null>":this.task));
        sb.append(',');
        sb.append("businessProcess");
        sb.append('=');
        sb.append(((this.businessProcess == null)?"<null>":this.businessProcess));
        sb.append(',');
        sb.append("message");
        sb.append('=');
        sb.append(((this.message == null)?"<null>":this.message));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.riskService == null)? 0 :this.riskService.hashCode()));
        result = ((result* 31)+((this.businessProcess == null)? 0 :this.businessProcess.hashCode()));
        result = ((result* 31)+((this.task == null)? 0 :this.task.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.message == null)? 0 :this.message.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ExecuteRiskServiceRs) == false) {
            return false;
        }
        ExecuteRiskServiceRs rhs = ((ExecuteRiskServiceRs) other);
        return ((((((this.riskService == rhs.riskService)||((this.riskService!= null)&&this.riskService.equals(rhs.riskService)))&&((this.businessProcess == rhs.businessProcess)||((this.businessProcess!= null)&&this.businessProcess.equals(rhs.businessProcess))))&&((this.task == rhs.task)||((this.task!= null)&&this.task.equals(rhs.task))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.message == rhs.message)||((this.message!= null)&&this.message.equals(rhs.message))));
    }

}
