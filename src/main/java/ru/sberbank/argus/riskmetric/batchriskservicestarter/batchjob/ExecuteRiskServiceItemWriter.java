package ru.sberbank.argus.riskmetric.batchriskservicestarter.batchjob;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.config.TaskProperties;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.model.ExecuteRiskServiceRq;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.model.ExecuteRiskServiceRs;

import java.util.List;

/**
 * @author Margaryan A.A.
 */
@Slf4j
@RequiredArgsConstructor
public class ExecuteRiskServiceItemWriter implements ItemWriter<ExecuteRiskServiceRq> {
    private final TaskProperties taskProperties;

    @Override
    public void write(List<? extends ExecuteRiskServiceRq> items) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String url = taskProperties.getUrl();
        for (ExecuteRiskServiceRq item : items) {
            String jsonRq = ow.writeValueAsString(item);
            log.info("Request:\n {}", jsonRq);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ExecuteRiskServiceRs> response = restTemplate.postForEntity(url, item, ExecuteRiskServiceRs.class);
            ExecuteRiskServiceRs responseBody = response.getBody();
            String jsonRs = ow.writeValueAsString(responseBody);
            log.info("Response:\n {}", jsonRs);
        }
    }
}
