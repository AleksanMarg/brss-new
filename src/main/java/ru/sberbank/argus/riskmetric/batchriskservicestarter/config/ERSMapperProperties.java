package ru.sberbank.argus.riskmetric.batchriskservicestarter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * @author Margaryan A.A.
 */
@Data
@Configuration
@EnableConfigurationProperties
@PropertySource(value ={"classpath:ers_mapper.yml"}, factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties("properties")
public class ERSMapperProperties {
    private List<ERSMapperProperty> propertyList;
    public static class ERSMapperProperty {
        private String sourceProperty;
        private String targetProperty;
        private String constantValue;

        public String getConstantValue() {
            return constantValue;
        }

        public void setConstantValue(String constantValue) {
            this.constantValue = constantValue;
        }

        public void setSourceProperty(String sourceProperty) {
            this.sourceProperty = sourceProperty;
        }

        public void setTargetProperty(String targetProperty) {
            this.targetProperty = targetProperty;
        }

        public String getSourceProperty() {
            return sourceProperty;
        }

        public String getTargetProperty() {
            return targetProperty;
        }
    }
}
