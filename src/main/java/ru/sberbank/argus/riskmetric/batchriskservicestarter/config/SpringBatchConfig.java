package ru.sberbank.argus.riskmetric.batchriskservicestarter.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.batchjob.ExecuteRiskServiceItemWriter;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.batchjob.RiskServiceItemProcessor;
import ru.sberbank.argus.riskmetric.batchriskservicestarter.model.ExecuteRiskServiceRq;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Margaryan A.A.
 */
@Slf4j
@Configuration
@EnableBatchProcessing
@RequiredArgsConstructor
public class SpringBatchConfig {
    private final TaskProperties taskProperties;
    private final ERSMapperProperties ersMapperProperties;
    private final JobBuilderFactory jobBuilderFactory;
    private final DataSource dataSource;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public JdbcCursorItemReader<Map<String, String>> reader() {
        JdbcCursorItemReader<Map<String, String>> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql(taskProperties.getSql());
        reader.setRowMapper(new MetricRowMapper());
        return reader;
    }


    public static class MetricRowMapper implements RowMapper<Map<String, String>> {
        @Override
        public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
            Map<String, String> sourceEntity = new HashMap<>();
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                sourceEntity.put(rs.getMetaData().getColumnName(i), rs.getString(i));
            }
            log.info("Row №{}: {}", rowNum, sourceEntity);
            return sourceEntity;
        }
    }

    @Bean
    public RiskServiceItemProcessor processor(ERSMapperProperties ersMapperProperties) {
        return new RiskServiceItemProcessor(ersMapperProperties);
    }


    @Bean
    public ExecuteRiskServiceItemWriter writer(TaskProperties taskProperties) {
        return new ExecuteRiskServiceItemWriter(taskProperties);
    }


    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .allowStartIfComplete(true)
                .<Map<String, String>, ExecuteRiskServiceRq>chunk(taskProperties.getStep1ChunkSize())
                .reader(reader())
                .processor(processor(ersMapperProperties))
                .writer(writer(taskProperties))
                .build();
    }

    @Bean
    public Job riskServiceEntityJob() {
        return jobBuilderFactory.get("riskServiceEntityJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }
}
