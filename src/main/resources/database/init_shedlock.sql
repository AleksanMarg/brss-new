declare
    v_sql LONG;
begin

    v_sql:='CREATE TABLE shedlock (
                          name VARCHAR(64),
                          lock_until TIMESTAMP(3) NULL,
                          locked_at TIMESTAMP(3) NULL,
                          locked_by VARCHAR(255),
                          PRIMARY KEY (name)
)';
    execute immediate v_sql;

EXCEPTION
    WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
            NULL;
        ELSE
            RAISE;
        END IF;
END;
/